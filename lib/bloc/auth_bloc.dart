import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:perona_new/model/user.model.dart';
import 'package:perona_new/services/auth.services.dart';
import 'package:perona_new/usecase/post_auth.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final PostAuth postAuth;
  AuthBloc(this.postAuth) : super(AuthInitial()) {
    on<OnLogin>(
      (event, emit) async {
        final username = event.email;
        final password = event.password;
        emit(AuthLoading());
        final result = await AuthServices.authUser(username, password);
        if (result.value != null) {
          SharedPreferences sp = await SharedPreferences.getInstance();

          String user = jsonEncode(result.value);

          sp.setString('_user', user);

          emit(AuthLoaded(result.value!));
        } else {
          emit(AuthError(result.message!));
        }
      },
      transformer: debounce(const Duration(milliseconds: 500)),
    );

    on<OnRegister>((event, emit) async {
      final params = event.params;
      emit(AuthLoading());
      final result = await AuthServices.registerUser(params);
      if (result.value != null) {
        SharedPreferences sp = await SharedPreferences.getInstance();

        String user = jsonEncode(result.value);

        sp.setString('_user', user);

        emit(AuthLoaded(result.value!));
      } else {
        emit(AuthError(result.message!));
      }
    });

    on<CheckSignIn>((event, emit) async {
      emit(AuthLoading());
      try {
        final result = await postAuth.executeLocal();
        if (result.value != null) {
          emit(AuthLoaded(result.value!));
        } else {
          emit(AuthSignOut());
        }
      } on Exception {
        emit(AuthSignOut());
      }
    });

    on<OnSignOut>((event, emit) async {
      try {
        SharedPreferences sp = await SharedPreferences.getInstance();
        sp.clear();
        emit(AuthSignOut());
      } on Exception {
        final result = await postAuth.executeLocal();
        if (result.value != null) {
          emit(AuthLoaded(result.value!));
        } else {
          emit(AuthSignOut());
        }
      }
    });
  }

  EventTransformer<T> debounce<T>(Duration duration) {
    return (events, mapper) => events.debounceTime(duration).flatMap(mapper);
  }
}
