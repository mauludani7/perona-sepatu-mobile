part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class OnLogin extends AuthEvent {
  final String email, password;
  const OnLogin(this.email, this.password);
  @override
  List<Object> get props => [email, password];
}

class OnRegister extends AuthEvent {
  final Map<String, String> params;
  const OnRegister(this.params);
  @override
  List<Object> get props => [params];
}

class OnSignOut extends AuthEvent {}

class CheckSignIn extends AuthEvent {}
