part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

class AuthInitial extends AuthState {}

class AuthLoading extends AuthState {}

class AuthSignOut extends AuthState {}

class AuthError extends AuthState {
  final String message;
  const AuthError(this.message);
  @override
  List<Object> get props => [message];
}

class AuthLoaded extends AuthState {
  final User user;

  const AuthLoaded(this.user);
  @override
  List<Object> get props => [user];
}

class AuthNotVerified extends AuthState {
   final User user;

  const AuthNotVerified(this.user);
  @override
  List<Object> get props => [user];
}