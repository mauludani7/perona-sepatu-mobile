import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:perona_new/model/user.model.dart';
import 'package:perona_new/services/transaksi.services.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'bukti_pembayaran_event.dart';
part 'bukti_pembayaran_state.dart';

class BuktiPembayaranBloc
    extends Bloc<BuktiPembayaranEvent, BuktiPembayaranState> {
  BuktiPembayaranBloc() : super(BuktiPembayaranInitial()) {
    on<OnSend>((event, emit) async {
      try {
        emit(BuktiPembayaranLoading());
        SharedPreferences sp = await SharedPreferences.getInstance();
        String userString = sp.getString('_user') ?? '';
        User? user;
        if (userString != '') {
          user = User.fromJson(jsonDecode(userString));
          final result = await TransaksiServices.postBuktiPembayaran(user.key,
              params: event.params);

          if (result.value != null) {
            emit(BuktiPembayaranSuccess());
          } else {
            emit(BuktiPembayaranError(result.message!));
          }
        } else {
          emit(const BuktiPembayaranError('User tidak ditemukan'));
        }
      } catch (e) {
        emit(BuktiPembayaranError(e.toString()));
      }
    });
  }
}
