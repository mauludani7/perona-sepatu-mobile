part of 'bukti_pembayaran_bloc.dart';

abstract class BuktiPembayaranEvent extends Equatable {
  const BuktiPembayaranEvent();

  @override
  List<Object> get props => [];
}

class OnSend extends BuktiPembayaranEvent {
  final Map<String, String> params;

  const OnSend(this.params);

  @override
  List<Object> get props => [params];
}
