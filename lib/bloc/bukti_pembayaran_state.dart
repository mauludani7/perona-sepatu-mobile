part of 'bukti_pembayaran_bloc.dart';

abstract class BuktiPembayaranState extends Equatable {
  const BuktiPembayaranState();

  @override
  List<Object> get props => [];
}

class BuktiPembayaranInitial extends BuktiPembayaranState {}

class BuktiPembayaranLoading extends BuktiPembayaranState {}

class BuktiPembayaranSuccess extends BuktiPembayaranState {}

class BuktiPembayaranError extends BuktiPembayaranState {
  final String message;
  const BuktiPembayaranError(this.message);

  @override
  List<Object> get props => [message];
}
