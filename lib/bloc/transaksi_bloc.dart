// ignore_for_file: avoid_print

import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:perona_new/model/user.model.dart';
import 'package:perona_new/model/transaksi.model.dart';
import 'package:perona_new/services/transaksi.services.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'transaksi_event.dart';
part 'transaksi_state.dart';

class TransaksiBloc extends Bloc<TransaksiEvent, TransaksiState> {
  TransaksiBloc() : super(TransaksiInitial()) {
    on<OnTransaksiEdit>((event, emit) {
      try {
        // emit(TransaksiLoading());
        Random random = Random();
        int randomId = random.nextInt(999999);
        state.initial!['layanan_antar'] = 'TIDAK';
        state.initial!['_id'] ??= [];
        state.initial!['_id'].add('$randomId');
        state.initial!['layanan_idlayanan'] ??= [];
        state.initial!['layanan_idlayanan']
            .add(event.detailTransaksi['layanan_idlayanan']);
        state.initial!['harga_layanan'] ??= [];
        state.initial!['harga_layanan']
            .add(event.detailTransaksi['harga_layanan']);
        state.initial!['nama_sepatu'] ??= [];
        state.initial!['nama_sepatu'].add(event.detailTransaksi['nama_sepatu']);
        state.initial!['warna_sepatu'] ??= [];
        state.initial!['warna_sepatu']
            .add(event.detailTransaksi['warna_sepatu']);
        state.initial!['jenis_sepatu'] ??= [];
        state.initial!['jenis_sepatu']
            .add(event.detailTransaksi['jenis_sepatu']);
        state.initial!['foto_sepatu'] ??= [];
        state.initial!['foto_sepatu'].add(event.detailTransaksi['foto_sepatu']);
        emit(TransaksiSet(state.initial!));
      } catch (e) {
        debugPrint(e.toString());
        emit(TransaksiError(e.toString(), initial: state.initial!));
      }
    });

    on<OnTransaksiAlamatEdit>((event, emit) {
      try {
        emit(TransaksiLoading(initial: state.initial!));
        var data = state.initial!;
        data['alamat_customer'] = event.alamat;
        emit(TransaksiSet(data));
      } catch (e) {
        debugPrint(e.toString());
        emit(TransaksiError(e.toString(), initial: state.initial!));
      }
    });

    on<OnTransaksiAntarEdit>((event, emit) {
      try {
        emit(TransaksiLoading(initial: state.initial!));
        var data = state.initial!;
        data['layanan_antar'] = event.antar ? 'YA' : 'TIDAK';
        emit(TransaksiSet(data));
      } catch (e) {
        debugPrint(e.toString());
        emit(TransaksiError(e.toString(), initial: state.initial!));
      }
    });

    on<OnDeleteTransaksi>((event, emit) {
      try {
        emit(TransaksiLoading(initial: state.initial!));
        var index = state.initial!['_id'].indexOf(event.idTransaksi);
        var data = state.initial!;
        data['_id'].removeAt(index);
        data['nama_sepatu'].removeAt(index);
        data['warna_sepatu'].removeAt(index);
        data['jenis_sepatu'].removeAt(index);
        data['foto_sepatu'].removeAt(index);
        data['harga_layanan'].removeAt(index);
        data['layanan_idlayanan'].removeAt(index);
        emit(TransaksiSet(data));
      } catch (e) {
        emit(TransaksiError(e.toString(), initial: state.initial!));
      }
    });

    on<OnPostTransaksi>((event, emit) async {
      emit(TransaksiLoading(initial: state.initial!));
      try {
        SharedPreferences sp = await SharedPreferences.getInstance();
        String userString = sp.getString('_user') ?? '';
        User? user;
        if (userString != '') {
          user = User.fromJson(jsonDecode(userString));
          var result = await TransaksiServices.postTransaksi(user.key,
              params: state.initial!);
          if (result.value != null) {
            emit(TransaksiSuccess(result.value!));
          } else {
            print('==> fail');
            emit(TransaksiError(result.message!, initial: state.initial!));
          }
        } else {
          emit(TransaksiError('User tidak ditemukan', initial: state.initial!));
        }
      } catch (e) {
        emit(TransaksiError(e.toString(), initial: state.initial!));
      }
    });

    on<OnResetTransaksi>((event, emit) async {
      emit(const TransaksiSet({}));
    });
  }
}
