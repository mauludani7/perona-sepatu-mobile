part of 'transaksi_bloc.dart';

abstract class TransaksiEvent extends Equatable {
  const TransaksiEvent();

  @override
  List<Object> get props => [];
}

class OnTransaksiEdit extends TransaksiEvent {
  final Map<String, dynamic> transaksi;
  final Map<String, dynamic> detailTransaksi;

  const OnTransaksiEdit(
      {required this.transaksi, required this.detailTransaksi});
  @override
  List<Object> get props => [transaksi, detailTransaksi];
}

class OnTransaksiAlamatEdit extends TransaksiEvent {
  final String alamat;

  const OnTransaksiAlamatEdit({required this.alamat});
  @override
  List<Object> get props => [alamat];
}

class OnTransaksiAntarEdit extends TransaksiEvent {
  final bool antar;

  const OnTransaksiAntarEdit({required this.antar});
  @override
  List<Object> get props => [antar];
}

class OnDeleteTransaksi extends TransaksiEvent {
  final String idTransaksi;

  const OnDeleteTransaksi(this.idTransaksi);
  @override
  List<Object> get props => [idTransaksi];
}

class OnPostTransaksi extends TransaksiEvent {}

class OnResetTransaksi extends TransaksiEvent {}
