// ignore_for_file: overridden_fields

part of 'transaksi_bloc.dart';

abstract class TransaksiState extends Equatable {
  final Map<String, dynamic>? initial;
  const TransaksiState({this.initial});

  @override
  List<Object?> get props => [initial];
}

class TransaksiInitial extends TransaksiState {
  TransaksiInitial() : super(initial: {});

  @override
  List<Object?> get props => [initial];
}

class TransaksiLoading extends TransaksiState {
  @override
  final Map<String, dynamic> initial;
  const TransaksiLoading({required this.initial}) : super(initial: initial);

  @override
  List<Object?> get props => [initial];
}

class TransaksiError extends TransaksiState {
  final String message;

  @override
  final Map<String, dynamic> initial;

  const TransaksiError(this.message, {required this.initial})
      : super(initial: initial);
  @override
  List<Object> get props => [message];
}

class TransaksiSuccess extends TransaksiState {
  final Transaksi transaksi;

  const TransaksiSuccess(this.transaksi);
  @override
  List<Object> get props => [transaksi];
}

class TransaksiSet extends TransaksiState {
  @override
  final Map<String, dynamic> initial;
  const TransaksiSet(this.initial) : super(initial: initial);
}
