import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:flutter_image_compress/flutter_image_compress.dart';

class ImageComponent {
  static Future<ImageModel?> pickImg(bool camera) async {
    final pickedFile = camera
        ? await ImagePicker().pickImage(source: ImageSource.camera)
        : await ImagePicker().pickImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      File imgFrom = File(pickedFile.path);

      final dir = await path_provider.getTemporaryDirectory();
      List<String> dataPath = pickedFile.path.split('/');

      String temporaryName =
          dataPath[dataPath.length - 1].replaceAll('.jpg', '');
      temporaryName = temporaryName.replaceAll('.png', '');
      temporaryName = temporaryName.replaceAll('.jpeg', '');
      temporaryName = "$temporaryName-temp.jpg";
      final targetPath = "${dir.absolute.path}/$temporaryName";

      XFile imgFrom2 = (await FlutterImageCompress.compressAndGetFile(
        pickedFile.path,
        targetPath,
        quality: 60,
        minWidth: 1000,
        minHeight: 1000,
        // rotate: 90,
      ))!;

      imgFrom = File(imgFrom2.path);

      final bytes = await imgFrom.readAsBytes();

      return ImageModel(
          file: imgFrom,
          encodeImages: base64.encode(bytes),
          // fileTemp: pickedFile.path
          fileTemp: targetPath);
    }
    return null;
  }

  static Widget widgetImgPick(Function callback,
      {required String title, bool camera = false}) {
    return SizedBox(
      child: DottedBorder(
        dashPattern: const [8, 4],
        strokeWidth: 2,
        color: Colors.grey,
        child: SizedBox(
          height: 260,
          child: Column(
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(left: 16, right: 16, top: 80),
                child: Center(
                    child: GestureDetector(
                  child: const Icon(
                    Icons.photo_camera,
                    color: Colors.grey,
                    size: 60,
                  ),
                  onTap: () async {
                    ImageModel? data = await pickImg(camera);
                    callback(data);
                  },
                )),
              ),
              const SizedBox(
                height: 5,
              ),
              Container(
                margin: const EdgeInsets.only(top: 6),
                child: const Center(
                  child: Text('import Image'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ImageModel {
  File file;
  String encodeImages;
  String fileTemp;

  ImageModel(
      {required this.file, required this.encodeImages, required this.fileTemp});
}
