import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:perona_new/component/image.dart';

class ChooseImagePage extends StatelessWidget {
  const ChooseImagePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: 200,
      child: Column(
        children: [
          GestureDetector(
            child: Container(
                color: Colors.white,
                padding: const EdgeInsets.all(16),
                child: const Column(children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Ambil Dari Kamera'),
                      Icon(Icons.chevron_right_sharp),
                    ],
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Divider()
                ])),
            onTap: () async {
              var img = await ImageComponent.pickImg(true);
              inspect(img);
            },
          ),
          GestureDetector(
            child: Container(
                color: Colors.white,
                padding: const EdgeInsets.all(16),
                child: const Column(children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Ambil Dari Gallery'),
                      Icon(Icons.chevron_right_sharp),
                    ],
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Divider()
                ])),
            onTap: () {
              ImageComponent.pickImg(false);
              // Get.back(result: "gallery");
            },
          )
        ],
      ),
    );
  }
}

class ImageInput {
  static Widget imgChoose(
      {required Function onChange, required BuildContext context}) {
    return Container(
      color: Colors.white,
      height: 200,
      child: Column(
        children: [
          GestureDetector(
            child: Container(
                color: Colors.white,
                padding: const EdgeInsets.all(16),
                child: const Column(children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Ambil Dari Kamera'),
                      Icon(Icons.chevron_right_sharp),
                    ],
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Divider()
                ])),
            onTap: () async {
              return onChange(await ImageComponent.pickImg(true));
            },
          ),
          GestureDetector(
            child: Container(
                color: Colors.white,
                padding: const EdgeInsets.all(16),
                child: const Column(children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Ambil Dari Gallery'),
                      Icon(Icons.chevron_right_sharp),
                    ],
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Divider()
                ])),
            onTap: () async {
              return onChange(await ImageComponent.pickImg(false));
            },
          )
        ],
      ),
    );
  }
}
