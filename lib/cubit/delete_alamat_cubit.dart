import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:perona_new/model/api_return_value.dart';
import 'package:perona_new/model/user.model.dart';
import 'package:perona_new/services/alamat.services.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'delete_alamat_state.dart';

class DeleteAlamatCubit extends Cubit<DeleteAlamatState> {
  DeleteAlamatCubit() : super(DeleteAlamatInitial());

  Future delete(String id) async {
    emit(DeleteAlamatLoading());
    try {
      SharedPreferences sp = await SharedPreferences.getInstance();
      String userString = sp.getString('_user') ?? '';
      User? user;
      if (userString != '') {
        user = User.fromJson(jsonDecode(userString));
        ApiReturnValue<String> result =
            await AlamatServices.deleteAlamat(user.key, id: id);
        if (result.value != null) {
          emit(DeleteAlamatSuccess(result.value!));
        } else {
          emit(DeleteAlamatError(result.message!));
        }
      } else {
        emit(const DeleteAlamatError('User tidak ditemukan'));
      }
    } catch (e) {
      emit(DeleteAlamatError(e.toString()));
    }
  }
}
