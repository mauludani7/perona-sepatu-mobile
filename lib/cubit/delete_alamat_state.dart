part of 'delete_alamat_cubit.dart';

abstract class DeleteAlamatState extends Equatable {
  const DeleteAlamatState();

  @override
  List<Object> get props => [];
}

class DeleteAlamatInitial extends DeleteAlamatState {}

class DeleteAlamatLoading extends DeleteAlamatState {}

class DeleteAlamatError extends DeleteAlamatState {
  final String message;
  const DeleteAlamatError(this.message);

  @override
  List<Object> get props => [message];
}

class DeleteAlamatSuccess extends DeleteAlamatState {
  final String message;
  const DeleteAlamatSuccess(this.message);

  @override
  List<Object> get props => [message];
}
