import 'dart:convert';

import 'package:perona_new/model/alamat.model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:perona_new/model/api_return_value.dart';
import 'package:perona_new/model/user.model.dart';
import 'package:perona_new/services/alamat.services.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'get_alamat_state.dart';

class GetAlamatCubit extends Cubit<GetAlamatState> {
  GetAlamatCubit() : super(GetAlamatInitial());

  Future getAlamat() async {
    emit(GetAlamatLoading());
    try {
      SharedPreferences sp = await SharedPreferences.getInstance();
      String userString = sp.getString('_user') ?? '';
      User? user;
      if (userString != '') {
        user = User.fromJson(jsonDecode(userString));
        ApiReturnValue<List<Alamat>> result =
            await AlamatServices.getList(user.key);
        if (result.value != null) {
          emit(GetAlamatSuccess(result.value!));
        } else {
          emit(GetAlamatError(result.message!));
        }
      } else {
        emit(const GetAlamatError('User tidak ditemukan'));
      }
    } catch (e) {
      emit(GetAlamatError(e.toString()));
    }
  }
}
