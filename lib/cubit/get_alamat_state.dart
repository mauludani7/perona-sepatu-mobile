part of 'get_alamat_cubit.dart';

abstract class GetAlamatState extends Equatable {
  const GetAlamatState();

  @override
  List<Object> get props => [];
}

class GetAlamatInitial extends GetAlamatState {}

class GetAlamatLoading extends GetAlamatState {}

class GetAlamatSuccess extends GetAlamatState {
  final List<Alamat> alamat;
  const GetAlamatSuccess(this.alamat);

  @override
  List<Object> get props => [alamat];
}

class GetAlamatError extends GetAlamatState {
  final String message;
  const GetAlamatError(this.message);

  @override
  List<Object> get props => [message];
}
