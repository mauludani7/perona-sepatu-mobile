import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:perona_new/model/api_return_value.dart';
import 'package:perona_new/model/layanan.model.dart';
import 'package:perona_new/model/user.model.dart';
import 'package:perona_new/services/layanan.services.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'get_layanan_state.dart';

class GetLayananCubit extends Cubit<GetLayananState> {
  GetLayananCubit() : super(GetLayananInitial());

  Future getLayanan() async {
    emit(GetLayananLoading());
    try {
      SharedPreferences sp = await SharedPreferences.getInstance();
      String userString = sp.getString('_user') ?? '';
      User? user;
      if (userString != '') {
        user = User.fromJson(jsonDecode(userString));
        ApiReturnValue<List<Layanan>> result =
            await LayananServices.getList(user.key);
        if (result.value != null) {
          emit(GetLayananSuccess(result.value!));
        } else {
          emit(GetLayananError(result.message!));
        }
      } else {
        emit(const GetLayananError('User tidak ditemukan'));
      }
    } catch (e) {
      emit(GetLayananError(e.toString()));
    }
  }
}
