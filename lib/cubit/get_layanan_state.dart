part of 'get_layanan_cubit.dart';

abstract class GetLayananState extends Equatable {
  const GetLayananState();

  @override
  List<Object> get props => [];
}

class GetLayananInitial extends GetLayananState {}
class GetLayananLoading extends GetLayananState {}
class GetLayananSuccess extends GetLayananState {
  final List<Layanan> layanan;
  const GetLayananSuccess(this.layanan);

  @override
  List<Object> get props => [layanan];
}
class GetLayananError extends GetLayananState {
  final String message;
  const GetLayananError(this.message);

  @override
  List<Object> get props => [message];
}


