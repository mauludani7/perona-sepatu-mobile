import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:perona_new/model/api_return_value.dart';
import 'package:perona_new/model/transaksi.model.dart';
import 'package:perona_new/model/user.model.dart';
import 'package:perona_new/services/transaksi.services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:equatable/equatable.dart';

part 'get_transaksi_state.dart';

class GetTransaksiCubit extends Cubit<GetTransaksiState> {
  GetTransaksiCubit() : super(GetTransaksiInitial());

  Future getTransaksi() async {
    emit(GetTransaksiLoading());
    try {
      SharedPreferences sp = await SharedPreferences.getInstance();
      String userString = sp.getString('_user') ?? '';
      User? user;
      if (userString != '') {
        user = User.fromJson(jsonDecode(userString));
        ApiReturnValue<List<Transaksi>> result =
            await TransaksiServices.gettransaksi(user.key);
        if (result.value != null) {
          emit(GetTransaksiSuccess(result.value!));
        } else {
          emit(GetTransaksiError(result.message!));
        }
      } else {
        emit(const GetTransaksiError('User tidak ditemukan'));
      }
    } catch (e) {
      emit(GetTransaksiError(e.toString()));
    }
  }
}
