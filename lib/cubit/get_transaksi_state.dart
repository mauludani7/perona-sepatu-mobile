part of 'get_transaksi_cubit.dart';

abstract class GetTransaksiState extends Equatable {
  const GetTransaksiState();

  @override
  List<Object> get props => [];
}

class GetTransaksiInitial extends GetTransaksiState {}

class GetTransaksiLoading extends GetTransaksiState {}

class GetTransaksiError extends GetTransaksiState {
  final String message;
  const GetTransaksiError(this.message);

  @override
  List<Object> get props => [message];
}

class GetTransaksiSuccess extends GetTransaksiState {
  final List<Transaksi> transaksi;

  const GetTransaksiSuccess(this.transaksi);

  @override
  List<Object> get props => [transaksi];
}
