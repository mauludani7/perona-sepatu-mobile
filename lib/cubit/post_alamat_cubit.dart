import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:perona_new/model/user.model.dart';
import 'package:perona_new/services/alamat.services.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'post_alamat_state.dart';

class PostAlamatCubit extends Cubit<PostAlamatState> {
  PostAlamatCubit() : super(PostAlamatInitial());

  Future post(String alamat) async {
    try {
      emit(PostAlamatLoading());
      SharedPreferences sp = await SharedPreferences.getInstance();
      String userString = sp.getString('_user') ?? '';
      User? user;
      if (userString != '') {
        user = User.fromJson(jsonDecode(userString));

        final result =
            await AlamatServices.postAlamat(user.key, alamat: alamat);
        if (result.value == null) {
          emit(PostAlamatSuccess());
        } else {
          emit(PostAlamatError(result.message!));
        }
      }
    } catch (e) {
      emit(PostAlamatError(e.toString()));
    }
  }
}
