part of 'post_alamat_cubit.dart';

abstract class PostAlamatState extends Equatable {
  const PostAlamatState();

  @override
  List<Object> get props => [];
}

class PostAlamatInitial extends PostAlamatState {}

class PostAlamatLoading extends PostAlamatState {}

class PostAlamatError extends PostAlamatState {
  final String message;
  const PostAlamatError(this.message);

  @override
  List<Object> get props => [message];
}

class PostAlamatSuccess extends PostAlamatState {}
