import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:perona_new/model/user.model.dart';
import 'package:perona_new/services/auth.services.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'post_pawword_state.dart';

class PostPawwordCubit extends Cubit<PostPawwordState> {
  PostPawwordCubit() : super(PostPawwordInitial());

  Future post(
      {required String baru,
      required String baruConfirm,
      required String lama}) async {
    try {
      emit(PostPawwordLoading());
      SharedPreferences sp = await SharedPreferences.getInstance();
      String userString = sp.getString('_user') ?? '';
      User? user;
      if (userString != '') {
        user = User.fromJson(jsonDecode(userString));

        final result = await AuthServices.updatePassword(user.key,
            lama: lama, baru: baru, baruConfirm: baruConfirm);
        if (result.value == null) {
          emit(PostPawwordSuccess());
        } else {
          emit(PostPawwordError(result.message!));
        }
      }
    } catch (e) {
      emit(PostPawwordError(e.toString()));
    }
  }
}
