part of 'post_pawword_cubit.dart';

abstract class PostPawwordState extends Equatable {
  const PostPawwordState();

  @override
  List<Object> get props => [];
}

class PostPawwordInitial extends PostPawwordState {}

class PostPawwordLoading extends PostPawwordState {}

class PostPawwordError extends PostPawwordState {
  final String message;
  const PostPawwordError(this.message);
}

class PostPawwordSuccess extends PostPawwordState {}
