import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:perona_new/model/user.model.dart';
import 'package:perona_new/services/layanan.services.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'status_pesanan_state.dart';

class StatusPesananCubit extends Cubit<StatusPesananState> {
  StatusPesananCubit() : super(StatusPesananInitial());

  Future getStatuPesanan() async {
    emit(StatusPesananLoading());
    try {
      
      SharedPreferences sp = await SharedPreferences.getInstance();
      String userString = sp.getString('_user') ?? '';
      User? user;
      if (userString != '') {
        user = User.fromJson(jsonDecode(userString));
        var result = await LayananServices.getStatusPesanan(user.key);
        if (result.value != null) {
          emit(StatusPesananSuccess(result.value!));
        } else {
          emit(StatusPesananError(result.message!));
        }
      } else {
        
      }
    } catch (e) {
      emit(StatusPesananError(e.toString()));
    }
  }
}
