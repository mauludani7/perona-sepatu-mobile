part of 'status_pesanan_cubit.dart';

abstract class StatusPesananState extends Equatable {
  const StatusPesananState();

  @override
  List<Object> get props => [];
}

class StatusPesananInitial extends StatusPesananState {}
class StatusPesananLoading extends StatusPesananState {}
class StatusPesananSuccess extends StatusPesananState {
  final Map<String, dynamic> result;
  const StatusPesananSuccess(this.result);

  @override
  List<Object> get props => [result];
}
class StatusPesananError extends StatusPesananState {
  final String message;
  const StatusPesananError(this.message);

  @override
  List<Object> get props => [message];
}
