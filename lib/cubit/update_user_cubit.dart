import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:perona_new/model/user.model.dart';
import 'package:perona_new/services/auth.services.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'update_user_state.dart';

class UpdateUserCubit extends Cubit<UpdateUserState> {
  UpdateUserCubit() : super(UpdateUserInitial());

  Future sendUpadte(Map<String, String> params) async {
    emit(UpdateUserLoading());

    try {
      SharedPreferences sp = await SharedPreferences.getInstance();
      String userString = sp.getString('_user') ?? '';
      User? user;
      if (userString != '') {
        user = User.fromJson(jsonDecode(userString));
        var result = await AuthServices.updateUser(user.key, params: params);
        if (result.value != null) {
          emit(UpdateUserSuccess());
        } else {
          emit(UpdateUserError(result.message!));
        }
      } else {
        emit(const UpdateUserError('User not found'));
      }
    } catch (e) {
      emit(UpdateUserError(e.toString()));
    }
  }
}
