import 'package:perona_new/bloc/auth_bloc.dart';
import 'package:perona_new/usecase/post_auth.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void init() {
  locator.registerFactory(() => AuthBloc(locator()));

  // usecase
  locator.registerLazySingleton(() => PostAuth());
}
