import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:perona_new/bloc/auth_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:perona_new/bloc/bukti_pembayaran_bloc.dart';
import 'package:perona_new/bloc/transaksi_bloc.dart';
import 'package:perona_new/cubit/delete_alamat_cubit.dart';
import 'package:perona_new/cubit/get_alamat_cubit.dart';
import 'package:perona_new/cubit/get_layanan_cubit.dart';
import 'package:perona_new/cubit/get_transaksi_cubit.dart';
import 'package:perona_new/cubit/post_alamat_cubit.dart';
import 'package:perona_new/cubit/post_pawword_cubit.dart';
import 'package:perona_new/cubit/status_pesanan_cubit.dart';
import 'package:perona_new/cubit/update_user_cubit.dart';
import 'package:perona_new/router/index.dart';
import 'injection.dart' as di;

void main() async {
  di.init();
  await dotenv.load(fileName: ".env");
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(create: (context) => GetLayananCubit()),
          BlocProvider(create: (context) => GetAlamatCubit()),
          BlocProvider(create: (context) => DeleteAlamatCubit()),
          BlocProvider(create: (context) => StatusPesananCubit()),
          BlocProvider(create: (context) => GetTransaksiCubit()),
          BlocProvider(create: (context) => TransaksiBloc()),
          BlocProvider(create: (context) => BuktiPembayaranBloc()),
          BlocProvider(create: (context) => UpdateUserCubit()),
          BlocProvider(create: (context) => PostAlamatCubit()),
          BlocProvider(create: (context) => PostPawwordCubit()),
          BlocProvider(
              create: (_) => di.locator<AuthBloc>()..add(CheckSignIn())),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.green,
            fontFamily: 'Urbanist',
          ),
          home: const Splash(),
        ));
  }
}

class Splash extends StatelessWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final GoRouter router = routerIndex;

    return AnimatedSplashScreen(
      splash: Column(
        children: [
          Image.asset('assets/images/LogoSplash.png'),
        ],
      ),
      splashIconSize: 400,
      backgroundColor: Colors.white,
      duration: 3000,
      splashTransition: SplashTransition.slideTransition,
      nextScreen: BlocListener<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state is AuthLoaded) {
            router.goNamed('dashboard');
          } else if (state is AuthSignOut) {
            router.goNamed('signin');
          }
        },
        child: MaterialApp.router(
          routeInformationProvider: router.routeInformationProvider,
          routeInformationParser: router.routeInformationParser,
          routerDelegate: router.routerDelegate,
          debugShowCheckedModeBanner: false,
        ),
      ),
    );
  }
}
