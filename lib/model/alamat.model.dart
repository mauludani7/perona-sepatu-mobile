class Alamat {
  Alamat({
    required this.idalamat,
    required this.alamat,
    required this.usersId,
    required this.alamatUtama,
  });
  late final String idalamat;
  late final String alamat;
  late final String usersId;
  late final String alamatUtama;

  Alamat.fromJson(Map<String, dynamic> json) {
    idalamat = json['idalamat'];
    alamat = json['alamat'];
    usersId = json['users_id'];
    alamatUtama = json['alamat_utama'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['idalamat'] = idalamat;
    data['alamat'] = alamat;
    data['users_id'] = usersId;
    data['alamat_utama'] = alamatUtama;
    return data;
  }
}
