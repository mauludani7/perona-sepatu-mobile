class Layanan {
  Layanan({
    required this.idlayanan,
    required this.namaLayanan,
    required this.durasiLayanan,
    required this.keteranganLayanan,
    required this.coverLayanan,
    required this.hargaLayanan,
  });
  late final String idlayanan;
  late final String namaLayanan;
  late final String durasiLayanan;
  late final String keteranganLayanan;
  late final String coverLayanan;
  late final String hargaLayanan;
  
  Layanan.fromJson(Map<String, dynamic> json){
    idlayanan = json['idlayanan'];
    namaLayanan = json['nama_layanan'];
    durasiLayanan = json['durasi_layanan'];
    keteranganLayanan = json['keterangan_layanan'];
    coverLayanan = json['cover_layanan'];
    hargaLayanan = json['harga_layanan'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['idlayanan'] = idlayanan;
    data['nama_layanan'] = namaLayanan;
    data['durasi_layanan'] = durasiLayanan;
    data['keterangan_layanan'] = keteranganLayanan;
    data['cover_layanan'] = coverLayanan;
    data['harga_layanan'] = hargaLayanan;
    return data;
  }
}