class Transaksi {
  Transaksi({
    required this.idtransaksi,
    required this.statusTransaksi,
    required this.tglTransaksi,
    this.alamatCustomer,
    this.layananAntar,
    required this.usersIdcustomer,
    required this.namaCustomer,
    this.totalHarga,
    required this.fotoBuktiPembayaran,
    required this.detileTransaksi,
  });
  late final String idtransaksi;
  late final String statusTransaksi;
  late final String tglTransaksi;
  late final String? alamatCustomer;
  late final String? layananAntar;
  late final String usersIdcustomer;
  late final String namaCustomer;
  late final String? totalHarga;
  late final String fotoBuktiPembayaran;
  late final List<DetileTransaksi> detileTransaksi;

  Transaksi.fromJson(Map<String, dynamic> json) {
    idtransaksi = json['idtransaksi'].toString();
    statusTransaksi = json['status_transaksi'];
    tglTransaksi = json['tgl_transaksi'];
    alamatCustomer = json['alamat_customer'] ?? '';
    layananAntar = json['layanan_antar'] ?? '';
    usersIdcustomer = json['users_idcustomer'].toString();
    namaCustomer = json['nama_customer'].toString();
    totalHarga = json['total_harga'] ?? '0';
    fotoBuktiPembayaran = json['foto_bukti_pembayaran'].toString();
    detileTransaksi = List.from(json['detile_transaksi'])
        .map((e) => DetileTransaksi.fromJson(e))
        .toList();
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['idtransaksi'] = idtransaksi;
    data['status_transaksi'] = statusTransaksi;
    data['tgl_transaksi'] = tglTransaksi;
    data['alamat_customer'] = alamatCustomer;
    data['layanan_antar'] = layananAntar;
    data['users_idcustomer'] = usersIdcustomer;
    data['nama_customer'] = namaCustomer;
    data['total_harga'] = totalHarga;
    data['foto_bukti_pembayaran'] = fotoBuktiPembayaran;
    data['detile_transaksi'] = detileTransaksi.map((e) => e.toJson()).toList();
    return data;
  }
}

class DetileTransaksi {
  DetileTransaksi({
    required this.iddetileTransaksi,
    required this.namaSepatu,
    required this.warnaSepatu,
    required this.jenisSepatu,
    required this.fotoSepatu,
    required this.layananIdlayanan,
    required this.transaksiIdtransaksi,
    required this.hargaLayanan,
  });
  late final String iddetileTransaksi;
  late final String namaSepatu;
  late final String warnaSepatu;
  late final String jenisSepatu;
  late final String fotoSepatu;
  late final String layananIdlayanan;
  late final String transaksiIdtransaksi;
  late final String hargaLayanan;

  DetileTransaksi.fromJson(Map<String, dynamic> json) {
    iddetileTransaksi = json['iddetile_transaksi'];
    namaSepatu = json['nama_sepatu'];
    warnaSepatu = json['warna_sepatu'];
    jenisSepatu = json['jenis_sepatu'];
    fotoSepatu = json['foto_sepatu'];
    layananIdlayanan = json['layanan_idlayanan'];
    transaksiIdtransaksi = json['transaksi_idtransaksi'];
    hargaLayanan = json['harga_layanan'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['iddetile_transaksi'] = iddetileTransaksi;
    data['nama_sepatu'] = namaSepatu;
    data['warna_sepatu'] = warnaSepatu;
    data['jenis_sepatu'] = jenisSepatu;
    data['foto_sepatu'] = fotoSepatu;
    data['layanan_idlayanan'] = layananIdlayanan;
    data['transaksi_idtransaksi'] = transaksiIdtransaksi;
    data['harga_layanan'] = hargaLayanan;
    return data;
  }
}
