class User {
  User({
    required this.id,
    required this.ipAddress,
    required this.username,
    required this.password,
    required this.email,
    required this.rememberSelector,
    required this.rememberCode,
    required this.createdOn,
    required this.lastLogin,
    required this.active,
    required this.firstName,
    required this.lastName,
    required this.company,
    required this.phone,
    required this.key,
    required this.userId,
  });
  late final String id;
  late final String ipAddress;
  late final String username;
  late final String password;
  late final String email;
  late final String rememberSelector;
  late final String rememberCode;
  late final String createdOn;
  late final String lastLogin;
  late final String active;
  late final String firstName;
  late final String lastName;
  late final String company;
  late final String phone;
  late final String key;
  late final String userId;

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    ipAddress = json['ip_address'];
    username = json['username'];
    password = json['password'];
    email = json['email'];
    rememberSelector = json['remember_selector'];
    rememberCode = json['remember_code'];
    createdOn = json['created_on'];
    lastLogin = json['last_login'];
    active = json['active'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    company = json['company'] ?? '';
    phone = json['phone'];
    key = json['key'];
    userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['ip_address'] = ipAddress;
    data['username'] = username;
    data['password'] = password;
    data['email'] = email;
    data['remember_selector'] = rememberSelector;
    data['remember_code'] = rememberCode;
    data['created_on'] = createdOn;
    data['last_login'] = lastLogin;
    data['active'] = active;
    data['first_name'] = firstName;
    data['last_name'] = lastName;
    data['company'] = company;
    data['phone'] = phone;
    data['key'] = key;
    data['user_id'] = userId;
    return data;
  }
}
