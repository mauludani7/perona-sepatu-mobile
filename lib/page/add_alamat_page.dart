import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:perona_new/cubit/get_alamat_cubit.dart';
import 'package:perona_new/cubit/post_alamat_cubit.dart';

class AddAlamatPage extends StatefulWidget {
  const AddAlamatPage({super.key});

  @override
  State<AddAlamatPage> createState() => _AddAlamatPageState();
}

class _AddAlamatPageState extends State<AddAlamatPage> {
  TextEditingController alamatC = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Tambah Alamat'),
      ),
      body: ListView(children: [
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
          height: 180,
          child: TextField(
            controller: alamatC,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Alamat Baru',
            ),
            maxLines: 3,
            style: const TextStyle(fontSize: 15),
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 24),
          child: BlocConsumer<PostAlamatCubit, PostAlamatState>(
            listener: (context, state) {
              if (state is PostAlamatError) {
                ArtSweetAlert.show(
                    context: context,
                    artDialogArgs: ArtDialogArgs(
                        type: ArtSweetAlertType.danger,
                        title: "Gagal!",
                        text: state.message));
              }
              if (state is PostAlamatSuccess) {
                context.read<GetAlamatCubit>().getAlamat();
                ArtSweetAlert.show(
                    context: context,
                    artDialogArgs: ArtDialogArgs(
                        type: ArtSweetAlertType.success,
                        title: "Berhasil!",
                        text: 'Alamat berhasil ditambahkan'));
                Navigator.pop(context);
              }
            },
            builder: (context, state) {
              if (state is PostAlamatLoading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              return ElevatedButton(
                  onPressed: () {
                    if (alamatC.text != '') {
                      context.read<PostAlamatCubit>().post(alamatC.text);
                    } else {
                      ArtSweetAlert.show(
                          context: context,
                          artDialogArgs: ArtDialogArgs(
                              type: ArtSweetAlertType.info,
                              title: "Gagal!",
                              text: 'Alamat harus di isi'));
                    }
                  },
                  child: const Text('Simpan Alamat'));
            },
          ),
        ),
      ]),
    );
  }
}
