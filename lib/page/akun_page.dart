import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:perona_new/bloc/auth_bloc.dart';
import 'package:perona_new/cubit/update_user_cubit.dart';

class AkunPage extends StatefulWidget {
  const AkunPage({Key? key}) : super(key: key);

  @override
  State<AkunPage> createState() => _AkunPageState();
}

class _AkunPageState extends State<AkunPage> {
  TextEditingController namaFirstC = TextEditingController();
  TextEditingController namaSecondC = TextEditingController();
  TextEditingController emailC = TextEditingController();
  TextEditingController nomorC = TextEditingController();
  String? namaF, namaS, email, nomor;
  List<String> dataAlamat = [
    'alamat 1',
    'alamat 2',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: BlocBuilder<AuthBloc, AuthState>(
        builder: (context, state) {
          if (state is AuthLoaded) {
            namaFirstC.text = state.user.firstName;
            namaF = state.user.firstName;
            namaSecondC.text = state.user.lastName;
            namaS = state.user.lastName;
            emailC.text = state.user.email;
            email = state.user.email;
            nomorC.text = state.user.phone;
            nomor = state.user.phone;
            return SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    width: 400,
                    height: 200,
                    decoration: const BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage("assets/images/bg_akun.png"),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Column(
                      children: <Widget>[
                        const SizedBox(
                          height: 15,
                        ),
                        const Text(
                          'Akun Anda',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontFamily: 'Urbanist',
                            fontSize: 28,
                            color: Colors.white,
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 6),
                          height: 100,
                          width: 100,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            color: Colors.white,
                            boxShadow: const [
                              BoxShadow(
                                color: Colors.black38,
                                offset: Offset(5.0, 0.0),
                                blurRadius: 5.0,
                              )
                            ],
                          ),
                          child: const Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Icon(Icons.face_6,
                                  size: 80, color: Colors.lightGreen),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 40,
                    width: 280,
                    child: TextField(
                      controller: namaFirstC,
                      decoration: const InputDecoration(
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        labelText: 'First Name',
                      ),
                      style: const TextStyle(fontSize: 15),
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(top: 15)),
                  SizedBox(
                    height: 40,
                    width: 280,
                    child: TextField(
                      controller: namaSecondC,
                      decoration: const InputDecoration(
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        labelText: 'Second Name',
                      ),
                      style: const TextStyle(fontSize: 15),
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(top: 15)),
                  SizedBox(
                    height: 40,
                    width: 280,
                    child: TextField(
                      readOnly: true,
                      controller: emailC,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Email',
                      ),
                      style: const TextStyle(fontSize: 15),
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(top: 15)),
                  SizedBox(
                    height: 40,
                    width: 280,
                    child: TextField(
                      controller: nomorC,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Nomor Handphone',
                      ),
                      style: const TextStyle(fontSize: 15),
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(top: 15)),
                  Container(
                    height: 40,
                    width: 280,
                    decoration: BoxDecoration(
                        color: Colors.lightGreen.shade400,
                        borderRadius: BorderRadius.circular(20)),
                    child: BlocConsumer<UpdateUserCubit, UpdateUserState>(
                      listener: (context, state) {
                        if (state is UpdateUserSuccess) {
                          ArtSweetAlert.show(
                              context: context,
                              artDialogArgs: ArtDialogArgs(
                                  type: ArtSweetAlertType.success,
                                  title: "Berhasil!",
                                  text: 'Berhasil diperbarui'));
                        }
                        if (state is UpdateUserError) {
                          ArtSweetAlert.show(
                              context: context,
                              artDialogArgs: ArtDialogArgs(
                                  type: ArtSweetAlertType.danger,
                                  title: "Gagal!",
                                  text: state.message));
                        }
                      },
                      builder: (context, state) {
                        if (state is UpdateUserLoading) {
                          return const Center(
                            child: CircularProgressIndicator(),
                          );
                        }
                        return TextButton(
                          onPressed: () {
                            Map<String, String> params = {
                              'first_name': namaFirstC.text,
                              'last_name': namaSecondC.text,
                              'phone': nomorC.text
                            };
                            context.read<UpdateUserCubit>().sendUpadte(params);
                          },
                          child: const Text(
                            'Update',
                            style: TextStyle(color: Colors.white, fontSize: 18),
                          ),
                        );
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 8),
                    height: 40,
                    width: 280,
                    decoration: BoxDecoration(
                        color: Colors.lightGreen.shade400,
                        borderRadius: BorderRadius.circular(20)),
                    child: TextButton(
                      onPressed: () {
                        context.goNamed('alamat');
                      },
                      child: const Text(
                        'Alamat',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 8),
                    height: 40,
                    width: 280,
                    decoration: BoxDecoration(
                        color: Colors.lightGreen.shade400,
                        borderRadius: BorderRadius.circular(20)),
                    child: TextButton(
                      onPressed: () {
                        context.goNamed('edit-password');
                      },
                      child: const Text(
                        'Ubah Password',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 8),
                    height: 40,
                    width: 280,
                    decoration: BoxDecoration(
                        color: Colors.lightGreen.shade400,
                        borderRadius: BorderRadius.circular(20)),
                    child: TextButton(
                      onPressed: () =>
                          {context.read<AuthBloc>().add(OnSignOut())},
                      child: const Text(
                        'Keluar',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(bottom: 15)),
                ],
              ),
            );
          }
          return const SizedBox();
        },
      ),
    );
  }
}
