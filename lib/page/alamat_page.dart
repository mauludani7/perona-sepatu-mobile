import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:perona_new/cubit/delete_alamat_cubit.dart';
import 'package:perona_new/cubit/get_alamat_cubit.dart';

class AlamatPage extends StatefulWidget {
  const AlamatPage({Key? key}) : super(key: key);

  @override
  State<AlamatPage> createState() => _AlamatPageState();
}

class _AlamatPageState extends State<AlamatPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Daftar Alamat'),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            context.goNamed('add-alamat1');
          },
          backgroundColor: Colors.green,
          child: const Icon(Icons.add),
        ),
        body: BlocBuilder<GetAlamatCubit, GetAlamatState>(
          builder: (context, state) {
            if (state is GetAlamatSuccess) {
              return ListView(
                children: state.alamat
                    .map((e) => Card(
                            child: BlocConsumer<DeleteAlamatCubit,
                                DeleteAlamatState>(
                          listener: (context, stateA) {
                            if (stateA is DeleteAlamatSuccess) {
                              ArtSweetAlert.show(
                                  context: context,
                                  artDialogArgs: ArtDialogArgs(
                                    type: ArtSweetAlertType.success,
                                    title: "Berhasil!",
                                    text: stateA.message,
                                  ));

                              context.read<GetAlamatCubit>().getAlamat();
                            }
                          },
                          builder: (context, state) {
                            return ListTile(
                              title: Text(e.alamat),
                              trailing: IconButton(
                                  onPressed: () {
                                    context
                                        .read<DeleteAlamatCubit>()
                                        .delete(e.idalamat);
                                  },
                                  icon: const Icon(Icons.delete)),
                              // subtitle: const Text('Utama')
                            );
                          },
                        )))
                    .toList(),
              );
            }
            return ListView(
              children: const [],
            );
          },
        ));
  }
}
