import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:perona_new/model/transaksi.model.dart';

class CheckOutPage extends StatefulWidget {
  final Transaksi transaksi;
  const CheckOutPage({super.key, required this.transaksi});

  @override
  State<CheckOutPage> createState() => _CheckOutPageState();
}

class _CheckOutPageState extends State<CheckOutPage> {
  @override
  Widget build(BuildContext context) {
    Widget header() {
      return Container(
        margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 24),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Customer',
                  style: TextStyle(color: Colors.grey),
                ),
                Text(widget.transaksi.namaCustomer)
              ],
            ),
            const SizedBox(
              height: 12,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Tanggal Transaksi',
                  style: TextStyle(color: Colors.grey),
                ),
                Text(widget.transaksi.tglTransaksi)
              ],
            ),
            const SizedBox(
              height: 12,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Alamat',
                  style: TextStyle(color: Colors.grey),
                ),
                Text(widget.transaksi.alamatCustomer!)
              ],
            ),
            const SizedBox(
              height: 12,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'Layanan Antar',
                  style: TextStyle(color: Colors.grey),
                ),
                Text(widget.transaksi.layananAntar!)
              ],
            ),
            const SizedBox(
              height: 12,
            ),
          ],
        ),
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.transaksi.statusTransaksi),
      ),
      body: ListView(
        children: [
          header(),
          Divider(
            color: Colors.black.withOpacity(0.5),
          ),
          Column(
            children: widget.transaksi.detileTransaksi
                .map((e) => Container(
                      margin: const EdgeInsets.symmetric(horizontal: 12),
                      child: Card(
                        child: ListTile(
                          leading: Image.network(e.fotoSepatu),
                          title: Text(e.namaSepatu),
                          subtitle: Text(
                              '${e.warnaSepatu}\n${e.jenisSepatu}\n${e.hargaLayanan}'),
                          isThreeLine: true,
                        ),
                      ),
                    ))
                .toList(),
          )
        ],
      ),
      bottomNavigationBar: widget.transaksi.statusTransaksi !=
              'MENUNGGU PEMBAYARAN'
          ? const SizedBox()
          : Container(
              padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
              height: 60,
              width: double.infinity,
              decoration: BoxDecoration(color: Colors.white, boxShadow: [
                BoxShadow(blurRadius: 6, color: Colors.black.withOpacity(0.2))
              ]),
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      const Text(
                        'Total Harga :',
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 16),
                      ),
                      const SizedBox(
                        width: 12,
                      ),
                      Text(
                        widget.transaksi.totalHarga!,
                        style: const TextStyle(
                            fontSize: 18,
                            color: Colors.red,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                  ElevatedButton(
                      onPressed: () {
                        context.goNamed('bukti-pembayaran',
                            extra: widget.transaksi);
                      },
                      child: const Text('Bukti Pembayaran'))
                ],
              ),
            ),
    );
  }
}
