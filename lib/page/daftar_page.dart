import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:perona_new/bloc/auth_bloc.dart';

class DaftarPage extends StatefulWidget {
  const DaftarPage({Key? key}) : super(key: key);

  @override
  State<DaftarPage> createState() => _DaftarPageState();
}

class _DaftarPageState extends State<DaftarPage> {
  TextEditingController namaF = TextEditingController();
  TextEditingController namaS = TextEditingController();
  TextEditingController email = TextEditingController();
  TextEditingController phone = TextEditingController();
  TextEditingController password = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        foregroundColor: Colors.lightGreen,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/icon.png',
              fit: BoxFit.contain,
              height: 50,
              width: 50,
            ),
            Container(
                padding: const EdgeInsets.all(0.0),
                child: const Text('Perona Sepatu'))
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            width: (MediaQuery.of(context).size.width),
            height: (MediaQuery.of(context).size.height) * 0.85,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bg.png"),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              children: [
                Container(
                  margin: const EdgeInsets.only(
                      top: 60, bottom: 30, left: 10, right: 10),
                  child: const Text(
                    'Daftar',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontFamily: 'Urbanist',
                      fontSize: 30,
                      color: Colors.lightGreen,
                    ),
                  ),
                ),
                Container(
                  height: 400,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30.0),
                    color: Colors.white54,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 20.0,
                          right: 20.0,
                          bottom: 15,
                        ),
                        child: SizedBox(
                          height: 45,
                          width: 300,
                          child: TextField(
                            controller: namaF,
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Nama Depan',
                                hintText: 'Masukkan Nama Depan anda'),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 20.0,
                          right: 20.0,
                          bottom: 15,
                        ),
                        child: SizedBox(
                          height: 45,
                          width: 300,
                          child: TextField(
                            controller: namaS,
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Nama Belakang',
                                hintText: 'Masukkan Nama Belakang anda'),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 20.0, right: 20.0, bottom: 15, top: 10),
                        child: SizedBox(
                          height: 45,
                          width: 300,
                          child: TextField(
                            controller: email,
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Email',
                                hintText: 'Ex : abc@gmail.com'),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 20.0, right: 20.0, bottom: 15, top: 10),
                        child: SizedBox(
                          height: 45,
                          width: 300,
                          child: TextField(
                            controller: phone,
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Nomor Handphone',
                                hintText: 'ex: 088123123123'),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            left: 20.0, right: 20.0, top: 10, bottom: 25),
                        child: SizedBox(
                          height: 45,
                          width: 300,
                          child: TextField(
                            controller: password,
                            obscureText: true,
                            decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: 'Password',
                                hintText: 'Masukkan Password Anda'),
                          ),
                        ),
                      ),
                      BlocConsumer<AuthBloc, AuthState>(
                        listener: (context, state) {
                          if (state is AuthError) {
                            ArtSweetAlert.show(
                                context: context,
                                artDialogArgs: ArtDialogArgs(
                                    type: ArtSweetAlertType.danger,
                                    title: "Gagal!",
                                    text: state.message));
                          }
                        },
                        builder: (context, state) {
                          if (state is AuthLoading) {
                            return const Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          return Container(
                            height: 40,
                            width: 280,
                            decoration: BoxDecoration(
                                color: Colors.lightGreen.shade400,
                                borderRadius: BorderRadius.circular(20)),
                            child: TextButton(
                              onPressed: () {
                                if (namaF.text != '' &&
                                    namaS.text != '' &&
                                    email.text != '' &&
                                    phone.text != '' &&
                                    password.text != '') {
                                  Map<String, String> params = {
                                    'first_name': namaF.text,
                                    'last_name': namaS.text,
                                    'identity': namaF.text,
                                    'email': email.text,
                                    'phone': phone.text,
                                    'password': password.text,
                                    'password_confirm': password.text,
                                  };
                                  context
                                      .read<AuthBloc>()
                                      .add(OnRegister(params));
                                } else {
                                  ArtSweetAlert.show(
                                      context: context,
                                      artDialogArgs: ArtDialogArgs(
                                          type: ArtSweetAlertType.info,
                                          title: "gagal!",
                                          text: "Masukan data dengan benar!"));
                                }
                              },
                              child: const Text(
                                'Daftar',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 20),
                              ),
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
