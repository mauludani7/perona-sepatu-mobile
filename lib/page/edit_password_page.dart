import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:perona_new/cubit/post_pawword_cubit.dart';

class EditPasswordPage extends StatefulWidget {
  const EditPasswordPage({super.key});

  @override
  State<EditPasswordPage> createState() => _EditPasswordPageState();
}

class _EditPasswordPageState extends State<EditPasswordPage> {
  TextEditingController passwordOldC = TextEditingController();
  TextEditingController passwordNewC = TextEditingController();
  TextEditingController passwordNewConfirmC = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Ubah Password'),
      ),
      body: ListView(children: [
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
          child: TextField(
            obscureText: true,
            controller: passwordOldC,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Password Lama',
            ),
            maxLines: 1,
            style: const TextStyle(fontSize: 15),
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
          child: TextField(
            obscureText: true,
            controller: passwordNewC,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Password Baru',
            ),
            maxLines: 1,
            style: const TextStyle(fontSize: 15),
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
          child: TextField(
            obscureText: true,
            controller: passwordNewConfirmC,
            decoration: const InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Confirm Password Baru',
            ),
            maxLines: 1,
            style: const TextStyle(fontSize: 15),
          ),
        ),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 24),
          child: BlocConsumer<PostPawwordCubit, PostPawwordState>(
            listener: (context, state) {
              if (state is PostPawwordError) {
                ArtSweetAlert.show(
                    context: context,
                    artDialogArgs: ArtDialogArgs(
                        type: ArtSweetAlertType.danger,
                        title: "Gagal!",
                        text: state.message));
              }
              if (state is PostPawwordSuccess) {
                ArtSweetAlert.show(
                    context: context,
                    artDialogArgs: ArtDialogArgs(
                        type: ArtSweetAlertType.success,
                        title: "Berhasil!",
                        text: 'Passwors berhasil diubah'));
                Navigator.pop(context);
              }
            },
            builder: (context, state) {
              if (state is PostPawwordLoading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              return ElevatedButton(
                  onPressed: () {
                    context.read<PostPawwordCubit>().post(
                        baru: passwordNewC.text,
                        baruConfirm: passwordNewConfirmC.text,
                        lama: passwordOldC.text);
                  },
                  child: const Text('Simpan Password'));
            },
          ),
        ),
      ]),
    );
  }
}
