import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:perona_new/bloc/auth_bloc.dart';
import 'package:perona_new/bloc/transaksi_bloc.dart';
import 'package:perona_new/cubit/status_pesanan_cubit.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Center(
        child: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/bg_nav.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Column(
            children: [
              const SizedBox(
                height: 15,
              ),
              const Text(
                "Halo",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Urbanist',
                  color: Colors.white,
                  fontSize: 20,
                  // fontWeight: FontWeight.w700,
                ),
              ),
              BlocBuilder<AuthBloc, AuthState>(
                builder: (context, state) {
                  if (state is AuthLoaded) {
                    return Text(
                      '${state.user.firstName} ${state.user.lastName}',
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontFamily: 'Urbanist',
                        color: Colors.white,
                        fontSize: 20,
                        // fontWeight: FontWeight.w700,
                      ),
                    );
                  }
                  return const SizedBox();
                },
              ),
              const SizedBox(height: 10.0),
              //
              //notif atas
              //
              const Text(
                "Proses Pesanan",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Urbanist',
                  color: Colors.white,
                  fontSize: 18,
                  // fontWeight: FontWeight.w700,
                ),
              ),

              //
              //box atas
              //

              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(40.0),
                  color: Colors.white54,
                  boxShadow: const [
                    BoxShadow(
                      color: Color.fromARGB(255, 216, 237, 192),
                      offset: Offset(0.1, 0.0),
                      blurRadius: 1.0,
                    )
                  ],
                ),
                margin: const EdgeInsets.only(top: 5, left: 15, right: 15),
                height: 120.0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  // This next line does the trick.
                  children: <Widget>[
                    BlocBuilder<StatusPesananCubit, StatusPesananState>(
                      builder: (context, state) {
                        if (state is StatusPesananSuccess) {
                          return SizedBox(
                            width: 100.0,
                            child: Column(
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(top: 18),
                                  height: 50,
                                  decoration: const BoxDecoration(
                                    image: DecorationImage(
                                      image: AssetImage(
                                          "assets/images/pickup.png"),
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                                const Text(
                                  'Belum Diambil',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: 'Urbanist',
                                    color: Color.fromARGB(255, 66, 157, 70),
                                    fontSize: 13,
                                  ),
                                ),
                                Text(
                                  state.result['belum_diambil'].toString(),
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                    fontFamily: 'Urbanist',
                                    color: Color.fromARGB(255, 66, 157, 70),
                                    fontSize: 16,
                                    // fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ],
                            ),
                          );
                        }
                        return const SizedBox();
                      },
                    ),
                    BlocBuilder<StatusPesananCubit, StatusPesananState>(
                      builder: (context, state) {
                        if (state is StatusPesananSuccess) {
                          return SizedBox(
                              width: 100.0,
                              child: Column(
                                children: [
                                  Container(
                                    margin: const EdgeInsets.only(top: 18),
                                    height: 50,
                                    decoration: const BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage(
                                            "assets/images/proses.png"),
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                  ),
                                  const Text(
                                    'Pengerjaan',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontFamily: 'Urbanist',
                                      color: Color.fromARGB(255, 66, 157, 70),
                                      fontSize: 13,
                                      // fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  Text(
                                    state.result['pengerjaan'].toString(),
                                    textAlign: TextAlign.center,
                                    style: const TextStyle(
                                      fontFamily: 'Urbanist',
                                      color: Color.fromARGB(255, 66, 157, 70),
                                      fontSize: 16,
                                      // fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                ],
                              ));
                        }
                        return const SizedBox();
                      },
                    ),
                    BlocBuilder<StatusPesananCubit, StatusPesananState>(
                      builder: (context, state) {
                        if (state is StatusPesananSuccess) {
                          return SizedBox(
                            width: 100.0,
                            child: Column(
                              children: [
                                Container(
                                  margin: const EdgeInsets.only(top: 18),
                                  height: 50,
                                  decoration: const BoxDecoration(
                                    image: DecorationImage(
                                      image:
                                          AssetImage("assets/images/antar.png"),
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                ),
                                const Text(
                                  'Diantar',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontFamily: 'Urbanist',
                                    color: Color.fromARGB(255, 66, 157, 70),
                                    fontSize: 13,
                                    // fontWeight: FontWeight.w700,
                                  ),
                                ),
                                Text(
                                  state.result['diantar'].toString(),
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(
                                    fontFamily: 'Urbanist',
                                    color: Color.fromARGB(255, 66, 157, 70),
                                    fontSize: 16,
                                  ),
                                ),
                              ],
                            ),
                          );
                        }
                        return const SizedBox();
                      },
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 5.0),

              //
              //slide bawah
              //

              Container(
                height: 20,
                color: Colors.transparent,
                child: const Text(
                  "Layanan Kami",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontFamily: 'Urbanist',
                    fontSize: 16,
                    // fontWeight: FontWeight.w700,
                    color: Colors.lightGreen,
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.all(2),
                height: 250.0,
                color: Colors.lightGreen.shade100,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: Colors.white,
                        boxShadow: const [
                          BoxShadow(
                            color: Colors.black12,
                            offset: Offset(3.0, 3.0),
                            blurRadius: 0.0,
                          )
                        ],
                      ),
                      margin: const EdgeInsets.all(5),
                      width: 250.0,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: Colors.white,
                        boxShadow: const [
                          BoxShadow(
                            color: Colors.black12,
                            offset: Offset(3.0, 3.0),
                            blurRadius: 0.0,
                          )
                        ],
                      ),
                      margin: const EdgeInsets.all(5),
                      width: 250.0,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: Colors.white,
                        boxShadow: const [
                          BoxShadow(
                            color: Colors.black12,
                            offset: Offset(3.0, 3.0),
                            blurRadius: 0.0,
                          )
                        ],
                      ),
                      margin: const EdgeInsets.all(5),
                      width: 250.0,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: Colors.white,
                        boxShadow: const [
                          BoxShadow(
                            color: Colors.black12,
                            offset: Offset(3.0, 3.0),
                            blurRadius: 0.0,
                          )
                        ],
                      ),
                      margin: const EdgeInsets.all(5),
                      width: 250.0,
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 10, bottom: 10),
                height: 40,
                width: 320,
                decoration: BoxDecoration(
                    color: Colors.lightGreen.shade400,
                    borderRadius: BorderRadius.circular(20)),
                child: TextButton(
                  onPressed: () {
                    var state = context.read<TransaksiBloc>().state;
                    if (state is TransaksiSet) {
                      context.goNamed('order-page');
                    } else {
                      context.goNamed('order-sepatu');
                    }
                  },
                  child: const Text(
                    'CUCI SEPATUMU SEKARANG JUGA!',
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
