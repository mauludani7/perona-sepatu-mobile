import 'package:flutter/material.dart';
import 'package:perona_new/cubit/get_alamat_cubit.dart';
import 'package:perona_new/cubit/get_layanan_cubit.dart';
import 'package:perona_new/cubit/get_transaksi_cubit.dart';
import 'package:perona_new/cubit/status_pesanan_cubit.dart';
import 'package:perona_new/page/home_page.dart';
import 'package:perona_new/page/akun_page.dart';
import 'package:perona_new/page/riwayat_page.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:perona_new/page/bantuan_page.dart';

class NavPage extends StatefulWidget {
  const NavPage({Key? key}) : super(key: key);

  @override
  State<NavPage> createState() => _NavPageState();
}

class _NavPageState extends State<NavPage> {
  int currentIndex = 0;
  final _halaman = [
    const HomePage(),
    const RiwayatPage(),
    const BantuanPage(),
    const AkunPage(),
  ];

  @override
  void initState() {
    init();
    super.initState();
  }

  Future init() async {
    context.read<GetTransaksiCubit>().getTransaksi();
    context.read<GetLayananCubit>().getLayanan();
    context.read<GetAlamatCubit>().getAlamat();
    context.read<StatusPesananCubit>().getStatuPesanan();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,

      appBar: AppBar(
        automaticallyImplyLeading: false,
        foregroundColor: Colors.lightGreen,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/icon.png',
              fit: BoxFit.contain,
              height: 50,
              width: 50,
            ),
            Container(
                padding: const EdgeInsets.all(0.0),
                child: const Text('Perona Sepatu'))
          ],
        ),
      ),
      body: IndexedStack(
        index: currentIndex,
        children: _halaman,
      ),

      //
      // navigasi bawah
      //
      bottomNavigationBar: GNav(
          padding: const EdgeInsetsDirectional.all(15),
          backgroundColor: Colors.white,
          gap: 2,
          color: const Color(0xFF8BC34A),
          activeColor: Colors.lightGreen,
          tabBackgroundColor: Colors.white,
          onTabChange: (index) => setState(() => currentIndex = index),
          tabs: const [
            GButton(
              margin: EdgeInsets.only(top: 5),
              icon: Icons.home,
              text: 'Home',
            ),
            GButton(
              margin: EdgeInsets.only(top: 5),
              icon: Icons.list,
              text: 'Riwayat',
            ),
            GButton(
              margin: EdgeInsets.only(top: 5),
              icon: Icons.question_mark,
              text: 'Bantuan',
            ),
            GButton(
              margin: EdgeInsets.only(top: 5),
              icon: Icons.person,
              text: 'Akun',
            ),
          ]),
    );
  }
}
