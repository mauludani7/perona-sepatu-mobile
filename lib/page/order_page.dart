import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:perona_new/bloc/transaksi_bloc.dart';
import 'package:perona_new/cubit/get_alamat_cubit.dart';
import 'package:perona_new/cubit/get_transaksi_cubit.dart';
import 'package:perona_new/cubit/status_pesanan_cubit.dart';
import 'package:perona_new/model/alamat.model.dart';
import 'nav_page.dart';
import 'order_sepatu.dart';

class OrderPage extends StatefulWidget {
  const OrderPage({Key? key}) : super(key: key);

  @override
  State<OrderPage> createState() => _OrderPageState();
}

class _OrderPageState extends State<OrderPage> {
  Alamat? alamatSelected;

  List<String> alamat = [
    'Alamat 1',
    'Alamat 2',
  ];

  List<String> nama = [];
  String? pilihAlamat = 'Alamat 1';

  @override
  void initState() {
    init();
    super.initState();
  }

  Future init() async {
    var state = context.read<TransaksiBloc>().state;

    if (state is TransaksiSet) {
      for (var i = 0; i < state.initial['nama_sepatu'].length; i++) {
        nama.add(state.initial['nama_sepatu'][i].toString());
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        foregroundColor: Colors.lightGreen,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/icon.png',
              fit: BoxFit.contain,
              height: 50,
              width: 50,
            ),
            Container(
                padding: const EdgeInsets.all(0.0),
                child: const Text('Perona Sepatu'))
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              width: 400,
              height: 180,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/images/bg_akun.png"),
                  fit: BoxFit.cover,
                ),
              ),
              child: const Column(
                children: <Widget>[
                  SizedBox(
                    height: 40,
                  ),
                  Text(
                    'Cuci Sepatumu ',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontFamily: 'Urbanist',
                      fontSize: 28,
                      color: Colors.white,
                    ),
                  ),
                  Text(
                    'Sekarang',
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontFamily: 'Urbanist',
                      fontSize: 28,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 25, right: 25),
              child: Column(
                children: [
                  BlocBuilder<TransaksiBloc, TransaksiState>(
                    builder: (context, state) {
                      if (state is TransaksiSet) {
                        if (state.initial['_id'] != null) {
                          return ListView(
                            physics: const NeverScrollableScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            children: (state.initial['nama_sepatu'] as Iterable)
                                .toList()
                                .asMap()
                                .map((i, e) => MapEntry(
                                    i,
                                    Card(
                                      shape: RoundedRectangleBorder(
                                        side: BorderSide(
                                            color: Colors.green.shade300),
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                      ),
                                      child: ListTile(
                                        title: Text(e),
                                        trailing: SizedBox(
                                          width: 30,
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: IconButton(
                                                    onPressed: () {
                                                      context
                                                          .read<TransaksiBloc>()
                                                          .add(
                                                              OnDeleteTransaksi(
                                                                  state.initial[
                                                                          '_id']
                                                                      [i]));
                                                    },
                                                    icon: const Icon(
                                                        Icons.delete)),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    )))
                                .values
                                .toList(),
                          );
                        }
                      }
                      return const SizedBox();
                    },
                  ),
                  IconButton(
                      color: Colors.lightGreen,
                      iconSize: 50,
                      onPressed: () => Navigator.push(
                            context,
                            PageRouteBuilder(
                              pageBuilder: (c, a1, a2) =>
                                  const OrderSepatuPage(),
                              transitionsBuilder: (c, anim, a2, child) =>
                                  FadeTransition(opacity: anim, child: child),
                              transitionDuration:
                                  const Duration(milliseconds: 1000),
                            ),
                          ),
                      icon: const Icon(Icons.add_circle)),
                ],
              ),
            ),
            SizedBox(
              // height: 57,
              width: 300,
              child: BlocBuilder<GetAlamatCubit, GetAlamatState>(
                builder: (context, state) {
                  if (state is GetAlamatSuccess) {
                    if (state.alamat.isNotEmpty) {
                      pilihAlamat = state.alamat[0].alamat;
                      alamatSelected = state.alamat[0];

                      context
                          .read<TransaksiBloc>()
                          .add(OnTransaksiAlamatEdit(alamat: pilihAlamat!));
                      return Column(
                        children: [
                          DropdownButtonFormField<String>(
                            decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(4),
                                  borderSide: const BorderSide(
                                      width: 1, color: Colors.lightGreen)),
                            ),
                            value: pilihAlamat,
                            items: state.alamat
                                .map(
                                  (item) => DropdownMenuItem<String>(
                                    value: item.alamat,
                                    child: Text(
                                      item.alamat,
                                      style: const TextStyle(fontSize: 15),
                                    ),
                                  ),
                                )
                                .toList(),
                            onChanged: (item) {
                              setState(() => pilihAlamat = item);
                              context
                                  .read<TransaksiBloc>()
                                  .add(OnTransaksiAlamatEdit(alamat: item!));
                              Alamat alamatS = state.alamat
                                  .where((e) => e.alamat == item)
                                  .first;
                              setState(() {
                                alamatSelected = alamatS;
                              });
                            },
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 8),
                            height: 40,
                            width: 280,
                            decoration: BoxDecoration(
                                color: Colors.lightGreen.shade400,
                                borderRadius: BorderRadius.circular(20)),
                            child: TextButton(
                              onPressed: () {},
                              child: const Text(
                                'Tambah Alamat',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18),
                              ),
                            ),
                          ),
                        ],
                      );
                    } else {
                      return Column(
                        children: [
                          Container(
                            height: 80,
                            width: double.infinity,
                            alignment: Alignment.center,
                            child: const Text(
                              'Alamat Kosong',
                              style: TextStyle(color: Colors.grey),
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 8),
                            height: 40,
                            width: 280,
                            decoration: BoxDecoration(
                                color: Colors.lightGreen.shade400,
                                borderRadius: BorderRadius.circular(20)),
                            child: TextButton(
                              onPressed: () {
                                context.goNamed('add-alamat2');
                              },
                              child: const Text(
                                'Tambah Alamat',
                                style: TextStyle(
                                    color: Colors.white, fontSize: 18),
                              ),
                            ),
                          ),
                        ],
                      );
                    }
                  }
                  return const SizedBox();
                },
              ),
            ),
            BlocBuilder<TransaksiBloc, TransaksiState>(
              builder: (context, state) {
                return Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 48, vertical: 12),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text(
                        'Layanan Antar ? ',
                        style: TextStyle(fontSize: 17.0),
                      ), //Text
                      const SizedBox(width: 10),
                      Checkbox(
                        value: true,
                        onChanged: (value) {
                          context
                              .read<TransaksiBloc>()
                              .add(OnTransaksiAntarEdit(antar: value!));
                        },
                      ),
                    ],
                  ),
                );
              },
            ),
            BlocConsumer<TransaksiBloc, TransaksiState>(
              listener: (context, state) {
                if (state is TransaksiSuccess) {
                  context.read<TransaksiBloc>().add(OnResetTransaksi());
                  context.read<GetTransaksiCubit>().getTransaksi();
                  context.read<GetAlamatCubit>().getAlamat();
                  context.read<StatusPesananCubit>().getStatuPesanan();

                  context.goNamed('detail', extra: state.transaksi);

                  ArtSweetAlert.show(
                      context: context,
                      artDialogArgs: ArtDialogArgs(
                          type: ArtSweetAlertType.success,
                          title: "Berhasil!",
                          text: ''));
                }
                // if (state is TransaksiError) {
                //   ArtSweetAlert.show(
                //       context: context,
                //       artDialogArgs: ArtDialogArgs(
                //           type: ArtSweetAlertType.danger,
                //           title: "Gagal!",
                //           text: state.message));
                // }
              },
              builder: (context, state) {
                if (state is TransaksiSet && alamatSelected != null) {
                  return Container(
                    margin: const EdgeInsets.only(top: 8),
                    height: 40,
                    width: 280,
                    decoration: BoxDecoration(
                        color: Colors.lightGreen.shade400,
                        borderRadius: BorderRadius.circular(20)),
                    child: TextButton(
                      onPressed: () {
                        if (state.initial['_id'].isEmpty &&
                            alamatSelected != null) {
                          ArtSweetAlert.show(
                              context: context,
                              artDialogArgs: ArtDialogArgs(
                                  type: ArtSweetAlertType.info,
                                  title: "Perhatian!",
                                  text: "Minimal ada 1 data"));
                        } else {
                          context.read<TransaksiBloc>().add(OnPostTransaksi());
                        }
                      },
                      child: const Text(
                        'Pembayaran',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                    ),
                  );
                }
                return const SizedBox();
              },
            ),
            Container(
              margin: const EdgeInsets.only(top: 8),
              height: 40,
              width: 280,
              decoration: BoxDecoration(
                  color: Colors.lightGreen.shade400,
                  borderRadius: BorderRadius.circular(20)),
              child: TextButton(
                onPressed: () => Navigator.push(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (c, a1, a2) => const NavPage(),
                    transitionsBuilder: (c, anim, a2, child) =>
                        FadeTransition(opacity: anim, child: child),
                    transitionDuration: const Duration(milliseconds: 1000),
                  ),
                ),
                child: const Text(
                  'Batalkan Pesanan',
                  style: TextStyle(color: Colors.white, fontSize: 18),
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
          ],
        ),
      ),
    );
  }
}
