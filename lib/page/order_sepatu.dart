import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:perona_new/bloc/transaksi_bloc.dart';
import 'package:perona_new/component/imagechoose.dart';
import 'package:perona_new/cubit/get_layanan_cubit.dart';
import 'package:perona_new/model/layanan.model.dart';
import 'order_page.dart';

class OrderSepatuPage extends StatefulWidget {
  const OrderSepatuPage({Key? key}) : super(key: key);

  @override
  State<OrderSepatuPage> createState() => _OrderSepatuState();
}

class _OrderSepatuState extends State<OrderSepatuPage> {
  TextEditingController namaSepatu = TextEditingController();
  TextEditingController warnaSepatu = TextEditingController();
  TextEditingController jenisSepatu = TextEditingController();
  Layanan? layananSelected;
  File? imgSepatu;
  String? extention;
  @override
  void initState() {
    super.initState();
  }

  String? pilihlayanan = 'Pilih Layanan';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        foregroundColor: Colors.lightGreen,
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/icon.png',
              fit: BoxFit.contain,
              height: 50,
              width: 50,
            ),
            Container(
                padding: const EdgeInsets.all(0.0),
                child: const Text('Perona Sepatu'))
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Container(
                width: 400,
                height: 180,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/bg_akun.png"),
                    fit: BoxFit.cover,
                  ),
                ),
                child: const Column(
                  children: <Widget>[
                    SizedBox(
                      height: 40,
                    ),
                    Text(
                      'Cuci Sepatumu ',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontFamily: 'Urbanist',
                        fontSize: 28,
                        color: Colors.white,
                      ),
                    ),
                    Text(
                      'Sekarang',
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontFamily: 'Urbanist',
                        fontSize: 28,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                // height: 400,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30.0),
                  color: Colors.white54,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 20.0,
                        right: 20.0,
                        bottom: 15,
                      ),
                      child: SizedBox(
                        height: 45,
                        width: 300,
                        child: TextField(
                          controller: namaSepatu,
                          decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Jenis Sepatu',
                              hintText: 'Masukkan merek dan jenis sepatu'),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, bottom: 15, top: 10),
                      child: SizedBox(
                        height: 45,
                        width: 300,
                        child: TextField(
                          controller: warnaSepatu,
                          decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Warna Sepatu',
                              hintText: 'Masukkan Warna Sepatu anda'),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, bottom: 15, top: 10),
                      child: SizedBox(
                        height: 45,
                        width: 300,
                        child: TextField(
                          controller: jenisSepatu,
                          decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              labelText: 'Ukuran Sepatu',
                              hintText: 'Masukkan Warna Jenis anda'),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 57,
                      width: 300,
                      child: BlocBuilder<GetLayananCubit, GetLayananState>(
                        builder: (context, state) {
                          if (state is GetLayananSuccess) {
                            if (state.layanan.isNotEmpty) {
                              pilihlayanan = state.layanan[0].namaLayanan;
                              layananSelected = state.layanan[0];
                              return DropdownButtonFormField<String>(
                                decoration: InputDecoration(
                                  enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(4),
                                      borderSide: const BorderSide(
                                          width: 1, color: Colors.lightGreen)),
                                ),
                                value: pilihlayanan,
                                items: state.layanan
                                    .map(
                                      (item) => DropdownMenuItem<String>(
                                        value: item.namaLayanan,
                                        child: Text(
                                          item.namaLayanan,
                                          style: const TextStyle(fontSize: 15),
                                        ),
                                      ),
                                    )
                                    .toList(),
                                onChanged: (item) {
                                  setState(() => pilihlayanan = item);
                                  Layanan layananS = state.layanan
                                      .where((e) => e.namaLayanan == item)
                                      .first;
                                  setState(() {
                                    layananSelected = layananS;
                                  });
                                },
                              );
                            }
                          }
                          return const SizedBox();
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, top: 10, bottom: 25),
                      child: GestureDetector(
                        onTap: () {
                          showModalBottomSheet(
                              context: context,
                              builder: (BuildContext context) {
                                return ImageInput.imgChoose(
                                    onChange: (val) {
                                      setState(() {
                                        imgSepatu = val.file;
                                        extention =
                                            val.fileTemp.split('.').last;
                                      });
                                      Navigator.pop(context);
                                      inspect(val);
                                    },
                                    context: context);
                              });
                        },
                        child: Container(
                          height: 45,
                          padding: const EdgeInsets.all(12),
                          width: 300,
                          alignment: Alignment.centerLeft,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(6),
                              border:
                                  Border.all(width: 1, color: Colors.black45)),
                          child: Text(imgSepatu == null
                              ? 'Upload Gambar'
                              : 'Gambar Terpilih'),
                        ),
                      ),
                    ),
                    Container(
                      height: 40,
                      width: 280,
                      decoration: BoxDecoration(
                          color: Colors.lightGreen.shade400,
                          borderRadius: BorderRadius.circular(20)),
                      child: TextButton(
                        onPressed: () {
                          // Map<String, dynamic> transaksi = {
                          //   'detile_transaksi': []
                          // };
                          Map<String, dynamic> detailTransaksi = {};

                          if (imgSepatu != null &&
                              namaSepatu.text != '' &&
                              warnaSepatu.text != '' &&
                              jenisSepatu.text != '') {
                            Map<String, dynamic> transaksi = {
                              'detile_transaksi': []
                            };

                            var byte = imgSepatu!.readAsBytesSync();
                            String imgBase64 = base64Encode(byte);

                            detailTransaksi = {
                              'nama_sepatu': namaSepatu.text,
                              'warna_sepatu': warnaSepatu.text,
                              'jenis_sepatu': jenisSepatu.text,
                              'harga_layanan': layananSelected!.hargaLayanan,
                              'layanan_idlayanan': layananSelected!.idlayanan,
                              'foto_sepatu':
                                  'data:image/$extention;base64,$imgBase64',
                            };

                            context.read<TransaksiBloc>().add(OnTransaksiEdit(
                                transaksi: transaksi,
                                detailTransaksi: detailTransaksi));
                            Navigator.push(
                                context,
                                PageRouteBuilder(
                                  pageBuilder: (c, a1, a2) => const OrderPage(),
                                  transitionsBuilder: (c, anim, a2, child) =>
                                      FadeTransition(
                                          opacity: anim, child: child),
                                  transitionDuration:
                                      const Duration(milliseconds: 1000),
                                ));
                          } else {
                            ArtSweetAlert.show(
                                context: context,
                                artDialogArgs: ArtDialogArgs(
                                    type: ArtSweetAlertType.info,
                                    title: "Perhatian!",
                                    text: "Lengkapi Form diatas!"));
                          }
                        },
                        child: const Text(
                          'Pesan',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Container(
                      height: 40,
                      width: 280,
                      decoration: BoxDecoration(
                          color: Colors.lightGreen.shade400,
                          borderRadius: BorderRadius.circular(20)),
                      child: TextButton(
                        onPressed: () => Navigator.pop(context),
                        child: const Text(
                          'Batalkan Pesanan',
                          style: TextStyle(color: Colors.white, fontSize: 20),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
