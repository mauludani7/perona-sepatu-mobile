import 'package:art_sweetalert/art_sweetalert.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:perona_new/bloc/bukti_pembayaran_bloc.dart';
import 'package:perona_new/component/image.dart';
import 'package:perona_new/cubit/get_transaksi_cubit.dart';
import 'package:perona_new/model/transaksi.model.dart';

class PembayanPage extends StatefulWidget {
  final Transaksi transaksi;
  const PembayanPage({super.key, required this.transaksi});

  @override
  State<PembayanPage> createState() => _PembayanPageState();
}

class _PembayanPageState extends State<PembayanPage> {
  ImageModel? imageSelected;

  String? extention;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Upload Bukti Pembayaran'),
      ),
      body: ListView(
        children: [
          Container(
            margin: const EdgeInsets.all(24),
            child: imageSelected != null
                ? Image.file(imageSelected!.file)
                : ImageComponent.widgetImgPick((img) {
                    // inspect(img);
                    setState(() {
                      imageSelected = img;
                      extention = img.fileTemp.split('.').last;
                    });
                  }, title: 'Upload Bukti Pembayaran'),
          ),
          imageSelected == null
              ? const SizedBox()
              : BlocConsumer<BuktiPembayaranBloc, BuktiPembayaranState>(
                  listener: (context, state) {
                    if (state is BuktiPembayaranSuccess) {
                      ArtSweetAlert.show(
                          context: context,
                          artDialogArgs: ArtDialogArgs(
                              type: ArtSweetAlertType.success,
                              title: "Berhasil!",
                              text: 'Berhasil dikirim'));

                      context.read<GetTransaksiCubit>().getTransaksi();

                      context.goNamed('dashboard');
                    }

                    if (state is BuktiPembayaranError) {
                      ArtSweetAlert.show(
                          context: context,
                          artDialogArgs: ArtDialogArgs(
                              type: ArtSweetAlertType.danger,
                              title: "Gagal!",
                              text: state.message));
                    }
                  },
                  builder: (context, state) {
                    if (state is BuktiPembayaranLoading) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    return Container(
                        margin: const EdgeInsets.all(24),
                        child: ElevatedButton(
                            onPressed: () {
                              Map<String, String> params = {
                                'idtransaksi': widget.transaksi.idtransaksi,
                                'foto_bukti_pembayaran':
                                    'data:image/$extention;base64,${imageSelected!.encodeImages}'
                              };

                              context
                                  .read<BuktiPembayaranBloc>()
                                  .add(OnSend(params));
                            },
                            child: const Text('Kirim Bukti Pembayaran')));
                  },
                ),
        ],
      ),
    );
  }
}
