import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:perona_new/cubit/get_transaksi_cubit.dart';

class RiwayatPage extends StatefulWidget {
  const RiwayatPage({Key? key}) : super(key: key);

  @override
  State<RiwayatPage> createState() => _RiwayatPageState();
}

class _RiwayatPageState extends State<RiwayatPage> {
  @override
  void initState() {
    init();
    super.initState();
  }

  Future init() async {
    context.read<GetTransaksiCubit>().getTransaksi();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: BlocBuilder<GetTransaksiCubit, GetTransaksiState>(
      builder: (context, state) {
        if (state is GetTransaksiSuccess) {
          return ListView(
            children: state.transaksi
                .map((e) => GestureDetector(
                      onTap: () {
                        context.goNamed('detail', extra: e);
                      },
                      child: Card(
                        child: ListTile(
                          leading:
                              Image.network(e.detileTransaksi[0].fotoSepatu),
                          title: Text(e.statusTransaksi),
                          subtitle: Text(
                              '${e.tglTransaksi}\nHarga : ${e.totalHarga}'),
                          isThreeLine: true,
                        ),
                      ),
                    ))
                .toList(),
          );
        }
        return ListView(
          children: const [],
        );
      },
    ));
  }
}
