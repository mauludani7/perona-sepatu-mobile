import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:perona_new/bloc/transaksi_bloc.dart';
import 'package:perona_new/model/transaksi.model.dart';
import 'package:perona_new/page/add_alamat_page.dart';
import 'package:perona_new/page/alamat_page.dart';
import 'package:perona_new/page/checkout_page.dart';
import 'package:perona_new/page/edit_password_page.dart';
import 'package:perona_new/page/login_page.dart';
import 'package:perona_new/page/nav_page.dart';
import 'package:perona_new/page/order_page.dart';
import 'package:perona_new/page/order_sepatu.dart';
import 'package:perona_new/page/pembayaran_page.dart';
import 'package:perona_new/page/riwayat_page.dart';

GoRouter routerIndex = GoRouter(
  routes: <GoRoute>[
    GoRoute(
        path: '/signin',
        name: 'signin',
        builder: (context, state) {
          return const LoginPage();
        },
        routes: const []),
    GoRoute(
        path: '/dashboard',
        name: 'dashboard',
        builder: (context, state) {
          return const NavPage();
        },
        routes: [
          GoRoute(
              path: 'detail',
              name: 'detail',
              builder: (context, state) {
                Object? obj;
                obj = state.extra;
                if (obj is Transaksi) {
                  return CheckOutPage(transaksi: obj);
                } else {
                  return const SizedBox();
                }
              },
              routes: [
                GoRoute(
                  path: 'bukti-pembayaran',
                  name: 'bukti-pembayaran',
                  builder: (context, state) {
                    Object? obj;
                    obj = state.extra;
                    if (obj is Transaksi) {
                      return PembayanPage(transaksi: obj);
                    } else {
                      return const SizedBox();
                    }
                  },
                )
              ]),
          GoRoute(
            path: 'riwayat',
            name: 'riwayat',
            builder: (context, state) {
              return const RiwayatPage();
            },
          ),
          GoRoute(
              path: 'alamat',
              name: 'alamat',
              builder: (context, state) {
                return const AlamatPage();
              },
              routes: [
                GoRoute(
                  path: 'add-alamat1',
                  name: 'add-alamat1',
                  builder: (context, state) {
                    return const AddAlamatPage();
                  },
                )
              ]),
          GoRoute(
            path: 'edit-password',
            name: 'edit-password',
            builder: (context, state) {
              return const EditPasswordPage();
            },
          ),
          GoRoute(
            path: 'order-sepatu',
            name: 'order-sepatu',
            builder: (context, state) {
              return const OrderSepatuPage();
            },
          ),
          GoRoute(
              path: 'order-page',
              name: 'order-page',
              builder: (context, state) {
                return const OrderPage();
              },
              routes: [
                GoRoute(
                  path: 'add-alamat2',
                  name: 'add-alamat2',
                  builder: (context, state) {
                    return const AddAlamatPage();
                  },
                )
              ]),
        ]),
  ],
  initialLocation: '/signin',
  debugLogDiagnostics: true,
  routerNeglect: true,
);
