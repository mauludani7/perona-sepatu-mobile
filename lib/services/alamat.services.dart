// ignore_for_file: avoid_print

import 'dart:convert';
import 'dart:developer';

import 'package:perona_new/model/alamat.model.dart';
import 'package:perona_new/model/api_return_value.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class AlamatServices {
  static Future<ApiReturnValue<List<Alamat>>> getList(String token,
      {http.Client? client}) async {
    client = http.Client();

    try {
      var url = Uri.parse("${dotenv.env['BASE_URL']}/alamat");

      print('>>>> otw backend : $url');
      var response = await client.get(
        url,
        headers: {"perona-api-key": token},
      );

      var data = jsonDecode(response.body);
      print(data.toString());
      if (response.statusCode != 200) {
        return ApiReturnValue(message: data['message']);
      }

      List<Alamat> result =
          (data as Iterable).map((e) => Alamat.fromJson(e)).toList();

      return ApiReturnValue(value: result);
    } catch (e) {
      inspect(e);
      return ApiReturnValue(message: e.toString());
    }
  }

  static Future<ApiReturnValue<List<Alamat>>> postAlamat(String token,
      {http.Client? client, required String alamat}) async {
    client = http.Client();

    try {
      var url = Uri.parse("${dotenv.env['BASE_URL']}/alamat");

      print('>>>> otw backend : $url');
      var response = await client.post(url,
          headers: {"perona-api-key": token},
          body: {"alamat_customer": alamat});

      var data = jsonDecode(response.body);
      print(data.toString());
      if (response.statusCode != 200) {
        return ApiReturnValue(message: data['message']);
      }

      List<Alamat> result =
          (data as Iterable).map((e) => Alamat.fromJson(e)).toList();

      return ApiReturnValue(value: result);
    } catch (e) {
      inspect(e);
      return ApiReturnValue(message: e.toString());
    }
  }

  static Future<ApiReturnValue<String>> deleteAlamat(String token,
      {http.Client? client, required String id}) async {
    client = http.Client();

    try {
      var url = Uri.parse("${dotenv.env['BASE_URL']}/alamat?id=$id");

      print('>>>> otw backend : $url');
      var response =
          await client.delete(url, headers: {"perona-api-key": token});

      var data = jsonDecode(response.body);
      if (response.statusCode != 200) {
        return ApiReturnValue(message: data['message']);
      }

      return ApiReturnValue(value: data['message']);
    } catch (e) {
      inspect(e);
      return ApiReturnValue(message: e.toString());
    }
  }
}
