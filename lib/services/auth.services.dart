// ignore_for_file: avoid_print

import 'dart:convert';
import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:perona_new/model/api_return_value.dart';
import 'package:perona_new/model/user.model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthServices {
  static Future<ApiReturnValue<User>> authUser(String username, String password,
      {http.Client? client}) async {
    client = http.Client();
    try {
      var url = Uri.parse("${dotenv.env['BASE_URL']}/customer_login");

      print('>>>> otw backend : $url');
      var response = await client.post(url,
          headers: {"Content-Type": "application/json"},
          body: jsonEncode(<String, String>{
            'username': username,
            'password': password,
          }));

      var data = jsonDecode(response.body);

      if (response.statusCode != 200) {
        return ApiReturnValue(message: data['message']);
      }

      SharedPreferences sp = await SharedPreferences.getInstance();

      sp.setString('_token', data['key']);

      User result = User.fromJson(data['data']);
      return ApiReturnValue(value: result);
    } catch (e) {
      return ApiReturnValue(message: e.toString());
    }
  }

  static Future<ApiReturnValue<String>> updateUser(String token,
      {http.Client? client, required Map<String, String> params}) async {
    client = http.Client();
    try {
      var url = Uri.parse("${dotenv.env['BASE_URL']}/ubahcustomer");

      print('>>>> otw backend : $url');
      var response = await client.post(url,
          headers: {'perona-api-key': token}, body: params);

      var data = jsonDecode(response.body);
      inspect(data);
      if (response.statusCode != 201) {
        return ApiReturnValue(message: data['message']);
      }

      return ApiReturnValue(value: data['message']);
    } catch (e) {
      return ApiReturnValue(message: e.toString());
    }
  }

  static Future<ApiReturnValue<User>> updatePassword(String token,
      {http.Client? client,
      required String lama,
      required String baru,
      required String baruConfirm}) async {
    client = http.Client();
    try {
      var url = Uri.parse("${dotenv.env['BASE_URL']}/change_password");

      print('>>>> otw backend : $url');
      var response = await client.post(url,
          headers: {'perona-api-key': token},
          body: {'new': baru, 'new_confirm': baruConfirm, 'old': lama});

      inspect(response);
      debugPrint(response.body.toString());

      var data = jsonDecode(response.body);

      if (response.statusCode != 200) {
        return ApiReturnValue(message: data['message']);
      }

      SharedPreferences sp = await SharedPreferences.getInstance();

      sp.setString('_token', data['key']);

      User result = User.fromJson(data['data']);
      return ApiReturnValue(value: result);
    } catch (e) {
      return ApiReturnValue(message: e.toString());
    }
  }

  static Future<ApiReturnValue<User>> registerUser(Map<String, String> params,
      {http.Client? client}) async {
    client = http.Client();
    // try {
    print('>>>> otw backend');
    var url = Uri.parse("${dotenv.env['BASE_URL']}/customer_register");
    var response = await client.post(url,
        headers: {"Content-Type": "application/x-www-form-urlencoded"},
        body: params);

    var data = jsonDecode(response.body);

    if (response.statusCode != 201) {
      return ApiReturnValue(message: data['message']);
    }

    SharedPreferences sp = await SharedPreferences.getInstance();

    sp.setString('_token', data['key']);

    User result = User.fromJson(data['data']);
    return ApiReturnValue(value: result);
    // } catch (e) {
    //   return ApiReturnValue(message: e.toString());
    // }
  }

  static Future<ApiReturnValue<User>> checkLogin() async {
    try {
      print('>>>>> check login');
      SharedPreferences sp = await SharedPreferences.getInstance();
      String user = sp.getString('_user') ?? '';
      print('>>>> user is : $user');
      if (user == '') {
        print('>>> user is null');
        return ApiReturnValue(message: 'error');
      }
      var result = jsonDecode(user);
      User resultUser = User.fromJson(result);
      return ApiReturnValue(value: resultUser);
    } catch (e) {
      return ApiReturnValue(message: e.toString());
    }
  }

  static Future<ApiReturnValue<User>> checkUser(
      String username, String password,
      {http.Client? client}) async {
    client = http.Client();
    try {
      var url = Uri.parse("${dotenv.env['API_URL']}/customer_login");
      print(url);
      var response = await client.post(url,
          headers: {"Content-Type": "application/json"},
          body: jsonEncode(<String, String>{
            'usename': username,
            'password': password,
          }));

      var data = jsonDecode(response.body);
      if (response.statusCode != 200) {
        return ApiReturnValue(message: data['data']['message']);
      }

      User warga = User.fromJson(data['data']['warga']);
      return ApiReturnValue(value: warga);
    } catch (e) {
      print(e.toString());
      return ApiReturnValue(message: e.toString());
    }
  }
}
