// ignore_for_file: avoid_print

import 'dart:convert';
import 'dart:developer';

import 'package:perona_new/model/api_return_value.dart';
import 'package:perona_new/model/layanan.model.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class LayananServices {
  static Future<ApiReturnValue<List<Layanan>>> getList(String token,
      {http.Client? client}) async {
    client = http.Client();

    try {
      var url = Uri.parse("${dotenv.env['BASE_URL']}/layanan");

      print('>>>> otw backend : $url');
      var response = await client.get(
        url,
        headers: {
          "perona-api-key": token},
      );

      var data = jsonDecode(response.body);
      if (response.statusCode != 200) {
        return ApiReturnValue(message: data['message']);
      }

      List<Layanan> result =
          (data as Iterable).map((e) => Layanan.fromJson(e)).toList();

      return ApiReturnValue(value: result);
    } catch (e) {
      inspect(e);
      return ApiReturnValue(message: e.toString());
    }
  }
  static Future<ApiReturnValue<Map<String, dynamic>>> getStatusPesanan(String token,
      {http.Client? client}) async {
    client = http.Client();

    try {
      var url = Uri.parse("${dotenv.env['BASE_URL']}/status_pesanan");

      print('>>>> otw backend : $url');
      var response = await client.get(
        url,
        headers: {
          "perona-api-key": token},
      );

      var data = jsonDecode(response.body);
      if (response.statusCode != 200) {
        return ApiReturnValue(message: data['message']);
      }



      return ApiReturnValue(value: data);
    } catch (e) {
      inspect(e);
      return ApiReturnValue(message: e.toString());
    }
  }
}
