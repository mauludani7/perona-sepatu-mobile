// ignore_for_file: avoid_print

import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:perona_new/model/api_return_value.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;
import 'package:perona_new/model/transaksi.model.dart';

class TransaksiServices {
  static Future<ApiReturnValue<Transaksi>> postTransaksi(String token,
      {http.Client? client, required Map<String, dynamic> params}) async {
    client = http.Client();

    // try {
    Map<String, String> paramsSend = {};
    for (var i = 0; i < params['nama_sepatu'].length; i++) {
      paramsSend['nama_sepatu[$i]'] = params['nama_sepatu'][i];
      paramsSend['warna_sepatu[$i]'] = params['warna_sepatu'][i];
      paramsSend['jenis_sepatu[$i]'] = params['jenis_sepatu'][i];
      paramsSend['foto_sepatu[$i]'] = params['foto_sepatu'][i];
      paramsSend['harga_layanan[$i]'] = params['harga_layanan'][i];
      paramsSend['layanan_idlayanan[$i]'] = params['layanan_idlayanan'][i];
    }

    paramsSend['alamat_customer'] = params['alamat_customer'];
    paramsSend['layanan_antar'] = params['layanan_antar'];

    var url = Uri.parse("${dotenv.env['BASE_URL']}/transaksi");

    print('>>>> otw backend post : $url');

    var response = await client.post(url,
        headers: {"perona-api-key": token}, body: paramsSend);

    var data = jsonDecode(response.body);
    debugPrint('==> res : $data');
    if (response.statusCode != 201) {
      return ApiReturnValue(message: data['message']);
    }

    Transaksi result = Transaksi.fromJson(data['data']);

    return ApiReturnValue(value: result);
    // } catch (e) {
    //   inspect(e);
    //   return ApiReturnValue(message: e.toString());
    // }
  }

  static Future<ApiReturnValue<String>> postBuktiPembayaran(String token,
      {http.Client? client, required Map<String, dynamic> params}) async {
    client = http.Client();

    try {
      var url = Uri.parse("${dotenv.env['BASE_URL']}/bukti_pembayaran");

      print('>>>> otw backend post : $url');

      var response = await client.post(url,
          headers: {"perona-api-key": token}, body: params);

      var data = jsonDecode(response.body);

      print('==> ${response.statusCode}');

      if (response.statusCode != 201) {
        return ApiReturnValue(message: data['message']);
      }
      return ApiReturnValue(value: 'success');
    } catch (e) {
      inspect(e);
      return ApiReturnValue(message: e.toString());
    }
  }

  static Future<ApiReturnValue<List<Transaksi>>> gettransaksi(String token,
      {http.Client? client}) async {
    client = http.Client();

    try {
      var url = Uri.parse("${dotenv.env['BASE_URL']}/transaksi");

      print('>>>> get : $url');
      var response = await client.get(
        url,
        headers: {"perona-api-key": token},
      );

      var data = jsonDecode(response.body);

      if (response.statusCode != 200) {
        return ApiReturnValue(message: data['message']);
      }

      List<Transaksi> result =
          (data as Iterable).map((e) => Transaksi.fromJson(e)).toList();

      return ApiReturnValue(value: result);
    } catch (e) {
      return ApiReturnValue(message: e.toString());
    }
  }
}
