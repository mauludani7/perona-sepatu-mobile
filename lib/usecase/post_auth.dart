
import 'package:perona_new/model/api_return_value.dart';
import 'package:perona_new/model/user.model.dart';
import 'package:perona_new/services/auth.services.dart';

class PostAuth {
  Future<ApiReturnValue<User>> execute(
          String username, String password) async =>
      await AuthServices.authUser(username, password);

  Future<ApiReturnValue<User>> executeLocal() async =>
      await AuthServices.checkLogin();
}
